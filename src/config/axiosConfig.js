import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://127.0.0.1:8091/v1/',
    timeout: 1000,
    params: {'user': localStorage.getItem('username')}
});

export default instance;