import React from "react";
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Login from "../pages/Login/Login";
import App from "../App";

function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Login}/>
                <Route path="/dash" component={App}/>
            </Switch>
        </BrowserRouter>
    );
}

export default Routes;