
import Header from "./components/header/header";
import Sidebar from "./components/sidebar/sidebar";
import Content from "./components/content/content";
import Footer from "./components/footer/footer";

function App() {
    return (
        <div className={"hold-transition sidebar-mini text-sm layout-footer-fixed layout-navbar-fixed"}>
            <div className={"wrapper"}>
                <Header></Header>
                <Sidebar></Sidebar>
                <Content></Content>
                <Footer></Footer>
            </div>

        </div>
    );
}

export default App;
