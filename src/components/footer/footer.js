import React from 'react';

const Footer = () => (
    <footer className="main-footer">
        <strong>Copyright © 2021-2023 <a href="https://danydev.co">danydev.co</a>.</strong>
        All rights reserved.
        <div className="float-right d-none d-sm-inline-block">
            <b>Version</b> 0.0.1
        </div>
    </footer>
);
export default Footer;
