import React from 'react';
import {Route} from "react-router-dom";
import Home from "../../pages/Home/Home";
import TeacherCreate from "../../pages/teacher/TeacherCreate/TeacherCreate";
import TeacherList from "../../pages/teacher/TeacherList/TeacherList";
import TeacherEdit from "../../pages/teacher/TeacherEdit/TeacherEdit";
import TeacherDelete from "../../pages/teacher/TeacherDelete/TeacherDelete";
import CurseDelete from "../../pages/course/CurseDelete/CurseDelete";
import CurseList from "../../pages/course/CurseList/CurseList";
import CurseEdit from "../../pages/course/CurseEdit/CurseEdit";
import CurseCreate from "../../pages/course/CurseCreate/CurseCreate";
import CourseAssignment from "../../pages/course/CourseAssignment/CourseAssignment";

const DashRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        path={`/dash${rest.path}`}
        render={props => (
            <Component {...props} />
        )}
    />
);

const Content = () => (
    <div className="content-wrapper">

        <DashRoute exact path="/" component={Home}/>

        <DashRoute exact path="/teacher/create" component={TeacherCreate}/>
        <DashRoute exact path="/teacher/list" component={TeacherList}/>
        <DashRoute exact path="/teacher/edit" component={TeacherEdit}/>
        <DashRoute exact path="/teacher/delete" component={TeacherDelete}/>

        <DashRoute exact path="/course/create" component={CurseCreate}/>
        <DashRoute exact path="/course/list" component={CurseList}/>
        <DashRoute exact path="/course/edit" component={CurseEdit}/>
        <DashRoute exact path="/course/delete" component={CurseDelete}/>
        <DashRoute exact path="/course/assignment" component={CourseAssignment}/>

    </div>
);
export default Content;
