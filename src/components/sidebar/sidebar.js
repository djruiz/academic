import React from 'react';
import {Link} from "react-router-dom";

const close = () => {
    localStorage.removeItem('username')
    window.location.href = "/";
};

const Sidebar = () => (
    <aside className="main-sidebar sidebar-dark-primary elevation-4">
        {/* Brand Logo */}
        <Link to="/" className="brand-link">
            <img src="/dist/img/AdminLTELogo.png" alt="Logo" className="brand-image img-circle elevation-3"
                 style={{opacity: '.8'}}/>
            <span className="brand-text font-weight-bold">ACADEMIC</span>
        </Link>
        <div className="sidebar">
            <nav className="mt-2">
                <ul className="nav nav-pills nav-sidebar flex-column nav-legacy nav-child-indent" data-widget="treeview"
                    role="menu" data-accordion="false">

                    <li className="nav-item menu-open">
                        <Link to="#" className="nav-link">
                            <i className="nav-icon fas fa-users"/>
                            <p>
                                Profesores
                                <i className="fas fa-angle-left right"/>
                            </p>
                        </Link>
                        <ul className="nav nav-treeview">
                            <li className="nav-item">
                                <Link to="/dash/teacher/create" className="nav-link">
                                    <i className="fa fa-plus nav-icon"/>
                                    <p>Crear profesor</p>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/dash/teacher/list" className="nav-link">
                                    <i className="fa fa-users nav-icon"/>
                                    <p>Listar profesores</p>
                                </Link>
                            </li>
                        </ul>
                    </li>
                    <li className="nav-item">
                        <Link to="#" className="nav-link">
                            <i className="nav-icon fas fa-table"/>
                            <p>Cursos
                                <i className="fas fa-angle-left right"/>
                            </p>
                        </Link>
                        <ul className="nav nav-treeview">
                            <li className="nav-item">
                                <Link to="/dash/course/create" className="nav-link">
                                    <i className="fa fa-plus nav-icon"/>
                                    <p>Crear curso</p>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/dash/course/list" className="nav-link">
                                    <i className="fa fa-list nav-icon"/>
                                    <p>Listar cursos</p>
                                </Link>
                            </li>
                        </ul>
                    </li>
                    <li className="nav-header">OTRAS OPCIONES</li>
                    <li className="nav-item">
                        <a className="nav-link" onClick={() => close()}>
                            <i className="nav-icon far fa-circle text-danger"></i>
                            <p className="text">Cerrar sesión</p>
                        </a>
                    </li>
                </ul>
            </nav>
            {/* /.sidebar-menu */}
        </div>
        {/* /.sidebar */}
    </aside>
);

Sidebar.propTypes = {};

Sidebar.defaultProps = {};

export default Sidebar;
