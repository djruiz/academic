import React, {useEffect, useState} from "react";
import axios from "../../../config/axiosConfig";
import {Link} from "react-router-dom";

const CourseAssignment = (props) => {

    const course = props.location.state.course;
    const [teachers, setTeachers] = useState([]);

    let [state, setState] = useState({
        show: false,
        type: "",
        msg: "",
    });


    useEffect(() => {
        axios.get('teachers/all')
            .then(response => setTeachers(response.data))
            .catch(error => console.error(error));
    }, []);

    const send = () => {
        setState({
            show: true,
            type: "alert-success",
            msg: "Buen trabajo, el curso ha sido asignado con éxito",
        })
    };

    return <>
        <section className="content-header">
            <div className="container-fluid">
                <div className="row mb-2">
                    <div className="col-sm-6">
                        <h1>Asignación de curso a profesor</h1>
                    </div>
                    <div className="col-sm-6">
                        <ol className="breadcrumb float-sm-right">
                            <li className="breadcrumb-item">
                                <Link to="/dash">Dasboard</Link>
                            </li>
                            <li className="breadcrumb-item">Curso</li>
                            <li className="breadcrumb-item active">Asignación</li>
                        </ol>
                    </div>
                </div>

                {state.show ?
                    <div className="row">
                        <div className="col-12">
                            <div className={`alert ${state.type} alert-dismissible`}>
                                <button type="button" className="close" data-dismiss="alert" aria-hidden="true">×
                                </button>
                                {state.msg}
                            </div>
                        </div>
                    </div> : ''}
            </div>
        </section>


        <section className="content">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <div className="card card-primary">
                            <div className="card-header">
                                <h3 className="card-title">Complete los datos solicitados</h3>
                            </div>
                            <form>
                                <div className="card-body">

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="courseCode">Código del curso</label>
                                                <input type="number" className="form-control" id="courseCode"
                                                       value={course.courseCode} readOnly/>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="name">Nombre del curso</label>
                                                <input type="text" className="form-control" id="name"
                                                       value={course.name} readOnly/>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="contractType">Profesor</label>
                                        <select className="custom-select" id="teacherId">
                                            <option>Seleccione</option>
                                            {teachers.map(teacher => (
                                                <option value={teacher.id}>{teacher.name}</option>
                                            ))}
                                        </select>
                                    </div>

                                </div>

                                <div className="card-footer">
                                    <button type="button" className="btn btn-primary" onClick={() => send()}>
                                        Asignar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </>
}

export default CourseAssignment;
