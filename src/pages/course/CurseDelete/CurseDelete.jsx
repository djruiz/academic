import React, {useState} from "react";
import axios from "../../../config/axiosConfig";
import {Link} from "react-router-dom";

const CurseDelete = (props) => {

    const course = props.location.state.course;

    const [formData, setFormData] = useState({
        courseCode: course.courseCode,
        name: course.name,
        totalHoursWeek: course.totalHoursWeek,
    });


    let [state, setState] = useState({
        show: false,
        type: "",
        msg: "",
    });

    const handleSubmit = async (event) => {
        event.preventDefault();
        await axios.delete("courses", {params: {'code': course.courseCode}})
            .then((response) => {
                window.location.href = "/dash/course/list";
            })
            .catch((error) => {
                const msg = error.response.status ? error.response.data.body : "A ocurrido un error inesperado intenta de nuevo"
                console.log(msg)
                setState({
                    show: true,
                    type: "alert-danger",
                    msg: msg,
                })
            });
    };

    return <>
        <section className="content-header">
            <div className="container-fluid">
                <div className="row mb-2">
                    <div className="col-sm-6">
                        <h1>Eliminar curso</h1>
                    </div>
                    <div className="col-sm-6">
                        <ol className="breadcrumb float-sm-right">
                            <li className="breadcrumb-item">
                                <Link to="/dash">Dasboard</Link>
                            </li>
                            <li className="breadcrumb-item">curso</li>
                            <li className="breadcrumb-item active">Eliminar</li>
                        </ol>
                    </div>
                </div>

                {state.show ?
                    <div className="row">
                        <div className="col-12">
                            <div className={`alert ${state.type} alert-dismissible`}>
                                <button type="button" className="close" data-dismiss="alert" aria-hidden="true">×
                                </button>
                                {state.msg}
                            </div>
                        </div>
                    </div> : ''}
            </div>
        </section>

        <section className="content">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <div className="card card-red">
                            <div className="card-header">
                                <h3 className="card-title">¿Esta seguro de eliminar este curso, recuerde que esta
                                    acción es irreversible?</h3>
                            </div>
                            <form onSubmit={handleSubmit}>
                                <div className="card-body">

                                    <div className="form-group">
                                        <label htmlFor="courseCode">Código del curso</label>
                                        <input type="number" className="form-control" id="courseCode"
                                               value={formData.courseCode} readOnly/>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="name">Nombre del curso</label>
                                                <input type="text" className="form-control" id="name"
                                                       value={formData.name} readOnly/>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="totalHoursWeek">Total de horas a la semana</label>
                                                <input type="number" className="form-control" id="totalHoursWeek"
                                                       value={formData.totalHoursWeek} readOnly/>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div className="card-footer">
                                    <button type="submit" className="btn btn-danger">
                                        Eliminar
                                    </button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </>
}

export default CurseDelete;
