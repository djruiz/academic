import React, {useEffect, useState} from "react";
import axios from "../../../config/axiosConfig";
import {Link} from "react-router-dom";

const CurseList = (props) => {
    const [courses, setCourses] = useState([]);

    useEffect(() => {
        axios.get('courses/all')
            .then(response => setCourses(response.data))
            .catch(error => console.error(error));
    }, []);

    return <>
        <section className="content-header">
            <div className="container-fluid">
                <div className="row mb-2">
                    <div className="col-sm-6">
                        <h1>Lista de cursos</h1>
                    </div>
                    <div className="col-sm-6">
                        <ol className="breadcrumb float-sm-right">
                            <li className="breadcrumb-item">
                                <Link to="/dash">Dasboard</Link>
                            </li>
                            <li className="breadcrumb-item">Curso</li>
                            <li className="breadcrumb-item active">Lista</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section className="content">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-header">
                                <h3 className="card-title">Cursos</h3>
                                <div className="card-tools">
                                    <div className="input-group input-group-sm" style={{width: 150}}>
                                        <input
                                            type="text"
                                            name="table_search"
                                            className="form-control float-right"
                                            placeholder="Search"
                                        />
                                        <div className="input-group-append">
                                            <button type="submit" className="btn btn-default">
                                                <i className="fas fa-search"/>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* /.card-header */}
                            <div className="card-body table-responsive p-0">
                                <table className="table table-hover text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>CÓDIGO</th>
                                            <th>NOMBRES</th>
                                            <th>HORAS EN LA SEMANA</th>
                                            <th className="text-center">OPCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {courses.map(course => (
                                            <tr key={course.id}>
                                                <td>{course.courseCode}</td>
                                                <td>{course.name}</td>
                                                <td>{course.totalHoursWeek}</td>
                                                <td className="project-actions text-center">
                                                    <Link to={{
                                                        pathname: '/dash/course/assignment',
                                                        state: {course: course}
                                                    }} className="btn btn-success btn-sm mr-2">
                                                        <i className="fas fa-user-plus mr-2"></i>
                                                        ASIGNAR
                                                    </Link>
                                                    <Link to={{
                                                        pathname: '/dash/course/edit',
                                                        state: {course: course}
                                                    }} className="btn btn-info btn-sm mr-2">
                                                        <i className="fas fa-pencil-alt mr-2"></i>
                                                        EDITAR
                                                    </Link>
                                                    <Link to={{
                                                        pathname: '/dash/course/delete',
                                                        state: {course: course}
                                                    }} className="btn btn-danger btn-sm mr-2" >
                                                        <i className="fas fa-trash mr-2"></i>
                                                        ELIMINAR
                                                    </Link>
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                            {/* /.card-body */}
                        </div>
                        {/* /.card */}
                    </div>
                </div>

            </div>
        </section>
    </>;
}

export default CurseList;
