import React, {Component} from 'react';
import axios from 'axios';

const baseUrl = "http://127.0.0.1:8091/v1/auth/create";

class Login extends Component {

    state = {
        form: {
            username: '',
            password: ''
        },
        error: {
            msg: 'Inicia sesión para iniciar tu sesión',
            state: false
        }
    }

    handleChange = async e => {
        await this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value
            }
        });
    }


    logIn = async () => {
        await axios.post(baseUrl, {username: this.state.form.username, password: this.state.form.password})
            .then(response => {
                localStorage.setItem('username', this.state.form.username)
                window.location.href = "./dash";
            })
            .catch(error => {
                if (error.response.status === 409) {
                    this.setState({
                        error: {
                            msg: error.response.data.body,
                            state: true,
                        }
                    });
                } else {
                    this.setState({
                        error: {
                            msg: 'Error en la solicitud',
                            state: true,
                        }
                    });
                }
            })

    }

    componentDidMount() {
        if (localStorage.getItem('username') != null) {
            window.location.href = "/dash";
        }
    }

    render() {
        return (

            <div className="hold-transition login-page">
                <div className="login-box">
                    <div className="login-logo">
                        <a href="/" className="text-bold">ACADEMIC</a>
                    </div>
                    {/* /.login-logo */}
                    <div className="card rounded-lg">
                        <div className="card-body login-card-body rounded-lg">
                            <p className={this.state.error.state ? "login-box-msg text-danger" : "login-box-msg"}>
                                {this.state.error.msg}
                            </p>
                            <form>
                                <div className="input-group mb-3">
                                    <input type="text" className="form-control"
                                           placeholder="Usuario"
                                           name="username"
                                           onChange={this.handleChange}/>
                                    <div className="input-group-append">
                                        <div className="input-group-text">
                                            <span className="fas fa-user"/>
                                        </div>
                                    </div>
                                </div>
                                <div className="input-group mb-3">
                                    <input type="password" className="form-control"
                                           placeholder="contraseñas"
                                           name="password"
                                           onChange={this.handleChange}/>
                                    <div className="input-group-append">
                                        <div className="input-group-text">
                                            <span className="fas fa-lock"/>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-7">
                                    </div>
                                    {/* /.col */}
                                    <div className="col-5">
                                        <button type="button"
                                                className="btn btn-primary btn-block"
                                                onClick={() => this.logIn()}>Iniciar sesión
                                        </button>
                                    </div>
                                    {/* /.col */}
                                </div>
                            </form>

                        </div>
                        {/* /.login-card-body */}
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;

