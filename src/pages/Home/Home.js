import React from 'react';
// import styles from './Home.module.scss';

const Home = () => (
    <section className="content">
        <h1>Dashboard</h1>
        <p>Optimice su planificación de cursos y asegúrese de que sus docentes tengan la carga de trabajo adecuada</p>
    </section>
);

export default Home;
