import React, {useState} from "react";
import axios from "../../../config/axiosConfig";
import {Link} from "react-router-dom";

const TeacherDelete = (props) => {

    const teacher = props.location.state.teacher;

    const [formData, setFormData] = useState({
        identification: teacher.identification,
        name: teacher.name,
        lastname: teacher.lastname,
    });


    let [state, setState] = useState({
        show: false,
        type: "",
        msg: "",
    });

    const handleSubmit = async (event) => {
        event.preventDefault();
        await axios.delete("teachers/delete", {params: {'identification': teacher.identification}})
            .then((response) => {
                window.location.href = "/dash/teacher/list";
            })
            .catch((error) => {
                const msg = error.response.status ? error.response.data.body : "A ocurrido un error inesperado intenta de nuevo"
                console.log(msg)
                setState({
                    show: true,
                    type: "alert-danger",
                    msg: msg,
                })
            });
    };

    return <>
        <section className="content-header">
            <div className="container-fluid">
                <div className="row mb-2">
                    <div className="col-sm-6">
                        <h1>Eliminar profesor</h1>
                    </div>
                    <div className="col-sm-6">
                        <ol className="breadcrumb float-sm-right">
                            <li className="breadcrumb-item">
                                <Link to="/dash">Dasboard</Link>
                            </li>
                            <li className="breadcrumb-item">profesor</li>
                            <li className="breadcrumb-item active">Actualizar</li>
                        </ol>
                    </div>
                </div>

                {state.show ?
                    <div className="row">
                        <div className="col-12">
                            <div className={`alert ${state.type} alert-dismissible`}>
                                <button type="button" className="close" data-dismiss="alert" aria-hidden="true">×
                                </button>
                                {state.msg}
                            </div>
                        </div>
                    </div> : ''}
            </div>
        </section>

        <section className="content">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <div className="card card-red">
                            <div className="card-header">
                                <h3 className="card-title">¿Esta seguro de eliminar este profesor, recuerde que esta
                                    acción es irreversible?</h3>
                            </div>
                            <form onSubmit={handleSubmit}>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="identification">Identificación</label>
                                        <input type="number" className="form-control" id="identification" readOnly
                                               value={formData.identification}/>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="name">Nombres</label>
                                        <input type="text" className="form-control" id="name" value={formData.name}
                                               readOnly/>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="lastname">Apellidos</label>
                                        <input type="text" className="form-control" id="lastname" readOnly
                                               value={formData.lastname}/>
                                    </div>

                                </div>


                                <div className="card-footer">
                                    <button type="submit" className="btn btn-danger">
                                        Eliminar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </>
}

export default TeacherDelete;
