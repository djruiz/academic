import React, {useState} from "react";
import {Link} from "react-router-dom";
import axios from '../../../config/axiosConfig';

const TeacherCreate = (props) => {

    const [formData, setFormData] = useState({
        identification: "",
        name: "",
        lastname: "",
        contractType: "",
        availableHours: "",
    });

    let [state, setState] = useState({
        show: false,
        type: "",
        msg: "",
    });

    const handleInputChange = (event) => {
        setFormData({
            ...formData,
            [event.target.id]: event.target.value,
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        await axios.post("teachers", formData)
            .then((response) => {
                setState({
                    show: true,
                    type: "alert-success",
                    msg: "Buen trabajo, el profesor ha sido registrado con éxito",
                })
            })
            .catch((error) => {
                const msg = error.response.status ? error.response.data.body : "A ocurrido un error inesperado intenta de nuevo"
                console.log(msg)
                setState({
                    show: true,
                    type: "alert-danger",
                    msg: msg,
                })
            });
    };

    return <>
        <section className="content-header">
            <div className="container-fluid">
                <div className="row mb-2">
                    <div className="col-sm-6">
                        <h1>Crear profesor</h1>
                    </div>
                    <div className="col-sm-6">
                        <ol className="breadcrumb float-sm-right">
                            <li className="breadcrumb-item">
                                <Link to="/dash">Dasboard</Link>
                            </li>
                            <li className="breadcrumb-item">profesor</li>
                            <li className="breadcrumb-item active">Crear</li>
                        </ol>
                    </div>
                </div>

                {state.show ?
                    <div className="row">
                        <div className="col-12">
                            <div className={`alert ${state.type} alert-dismissible`}>
                                <button type="button" className="close" data-dismiss="alert" aria-hidden="true">×
                                </button>
                                {state.msg}
                            </div>
                        </div>
                    </div> : ''}
            </div>
        </section>


        <section className="content">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <div className="card card-primary">
                            <div className="card-header">
                                <h3 className="card-title">Complete los datos solicitados</h3>
                            </div>
                            <form onSubmit={handleSubmit}>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="identification">Identificación</label>
                                        <input type="number" className="form-control" id="identification"
                                               value={formData.identification} onChange={handleInputChange}/>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="name">Nombres</label>
                                        <input type="text" className="form-control" id="name" value={formData.name}
                                               onChange={handleInputChange}/>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="lastname">Apellidos</label>
                                        <input type="text" className="form-control" id="lastname"
                                               value={formData.lastname} onChange={handleInputChange}/>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="contractType">Tipo de contrato</label>
                                                <select className="custom-select" id="contractType"
                                                        value={formData.contractType} onChange={handleInputChange}>
                                                    <option>Seleccione</option>
                                                    <option value="fulltime">Tiempo completo</option>
                                                    <option value="halftime">Medio tiempo</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="availableHours">Horas disponibles</label>
                                                <select className="custom-select"
                                                        id="availableHours" value={formData.availableHours}
                                                        onChange={handleInputChange}>
                                                    <option>Seleccione</option>
                                                    <option value="20">20</option>
                                                    <option value="40">40</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">
                                        Registrar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </>

}

export default TeacherCreate;
