import React, {useState} from "react";
import axios from "../../../config/axiosConfig";
import {Link} from "react-router-dom";

const TeacherEdit = (props) => {

    const teacher = props.location.state.teacher;

    const [formData, setFormData] = useState({
        identification: teacher.identification,
        name: teacher.name,
        lastname: teacher.lastname,
        contractType: teacher.contractType,
        availableHours: teacher.availableHours,
        state: teacher.active,
    });

    let [state, setState] = useState({
        show: false,
        type: "",
        msg: "",
    });

    const handleInputChange = (event) => {
        setFormData({
            ...formData,
            [event.target.id]: event.target.value,
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        await axios.put("teachers", formData, {
            params: {'identification': formData.identification}
        })
            .then((response) => {
                setState({
                    show: true,
                    type: "alert-success",
                    msg: "Buen trabajo, el profesor ha sido actualizado con éxito",
                })
            })
            .catch((error) => {
                const msg = error.response.status ? error.response.data.body : "A ocurrido un error inesperado intenta de nuevo"
                console.log(msg)
                setState({
                    show: true,
                    type: "alert-danger",
                    msg: msg,
                })
            });
    };

    return <>
        <section className="content-header">
            <div className="container-fluid">
                <div className="row mb-2">
                    <div className="col-sm-6">
                        <h1>Actualizar profesor</h1>
                    </div>
                    <div className="col-sm-6">
                        <ol className="breadcrumb float-sm-right">
                            <li className="breadcrumb-item">
                                <Link to="/dash">Dasboard</Link>
                            </li>
                            <li className="breadcrumb-item">profesor</li>
                            <li className="breadcrumb-item active">Actualizar</li>
                        </ol>
                    </div>
                </div>

                {state.show ?
                    <div className="row">
                        <div className="col-12">
                            <div className={`alert ${state.type} alert-dismissible`}>
                                <button type="button" className="close" data-dismiss="alert" aria-hidden="true">×
                                </button>
                                {state.msg}
                            </div>
                        </div>
                    </div> : ''}
            </div>
        </section>


        <section className="content">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <div className="card card-primary">
                            <div className="card-header">
                                <h3 className="card-title">Digíte los datos a actualizar</h3>
                            </div>
                            <form onSubmit={handleSubmit}>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="identification">Identificación</label>
                                        <input type="number" className="form-control" id="identification"
                                               value={formData.identification} onChange={handleInputChange}/>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="name">Nombres</label>
                                        <input type="text" className="form-control" id="name" value={formData.name}
                                               onChange={handleInputChange}/>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="lastname">Apellidos</label>
                                        <input type="text" className="form-control" id="lastname"
                                               value={formData.lastname} onChange={handleInputChange}/>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="contractType">Tipo de contrato</label>
                                                <select className="custom-select" id="contractType"
                                                        value={formData.contractType} onChange={handleInputChange}>
                                                    <option>Seleccione</option>
                                                    <option value="fulltime">Tiempo completo</option>
                                                    <option value="halftime">Medio tiempo</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="availableHours">Horas disponibles</label>
                                                <select className="custom-select"
                                                        id="availableHours" value={formData.availableHours}
                                                        onChange={handleInputChange}>
                                                    <option>Seleccione</option>
                                                    <option value="20">20</option>
                                                    <option value="40">40</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="state">Estado</label>
                                        <select className="custom-select " id="state" value={formData.state}
                                                onChange={handleInputChange}>
                                            <option>Seleccione</option>
                                            <option value="true">Activo</option>
                                            <option value="false">Inactivo</option>
                                        </select>
                                    </div>
                                </div>


                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">
                                        Actualizar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </>

}

export default TeacherEdit;
