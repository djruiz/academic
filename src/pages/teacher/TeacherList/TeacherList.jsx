import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import axios from '../../../config/axiosConfig';

const TeacherList = (props) => {

    const [teachers, setTeachers] = useState([]);

    useEffect(() => {
        axios.get('teachers/all')
            .then(response => setTeachers(response.data))
            .catch(error => console.error(error));
    }, []);

    return <>
        <section className="content-header">
            <div className="container-fluid">
                <div className="row mb-2">
                    <div className="col-sm-6">
                        <h1>Lista de profesores</h1>
                    </div>
                    <div className="col-sm-6">
                        <ol className="breadcrumb float-sm-right">
                            <li className="breadcrumb-item">
                                <Link to="/dash">Dasboard</Link>
                            </li>
                            <li className="breadcrumb-item">profesor</li>
                            <li className="breadcrumb-item active">Lista</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section className="content">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-header">
                                <h3 className="card-title">Profesores</h3>
                                <div className="card-tools">
                                    <div className="input-group input-group-sm" style={{width: 150}}>
                                        <input
                                            type="text"
                                            name="table_search"
                                            className="form-control float-right"
                                            placeholder="Search"
                                        />
                                        <div className="input-group-append">
                                            <button type="submit" className="btn btn-default">
                                                <i className="fas fa-search"/>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* /.card-header */}
                            <div className="card-body table-responsive p-0">
                                <table className="table table-hover text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>NOMBRES</th>
                                            <th>APELLIDOS</th>
                                            <th>TIPO CONTRATO</th>
                                            <th>HORAS DISPONIBLES</th>
                                            <th>ESTADO</th>
                                            <th className="text-center">OPCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {teachers.map(teacher => (
                                            <tr key={teacher.identification}>
                                                <td>{teacher.identification}</td>
                                                <td>{teacher.name}</td>
                                                <td>{teacher.lastname}</td>
                                                <td>{teacher.contractType}</td>
                                                <td>{teacher.availableHours}</td>
                                                <td>{teacher.active ? 'Activo' : 'Inactivo'}</td>
                                                <td className="project-actions text-center">
                                                    <Link to={{
                                                        pathname: '/dash/teacher/edit',
                                                        state: {teacher: teacher}
                                                    }} className="btn btn-info btn-sm mr-2">
                                                        <i className="fas fa-pencil-alt mr-2"></i>
                                                        EDITAR
                                                    </Link>
                                                    <Link to={{
                                                        pathname: '/dash/teacher/delete',
                                                        state: {teacher: teacher}
                                                    }} className="btn btn-danger btn-sm mr-2" >
                                                        <i className="fas fa-trash mr-2"></i>
                                                        ELIMINAR
                                                    </Link>
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                            {/* /.card-body */}
                        </div>
                        {/* /.card */}
                    </div>
                </div>

            </div>
        </section>
    </>;
}

export default TeacherList;
